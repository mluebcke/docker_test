<?php /* Smarty version Smarty-3.1.12, created on 2018-10-06 13:38:55
         compiled from "/var/www/html/themes/Frontend/Bare/frontend/plugins/seo/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8706001165bb89ecfcd4876-53906141%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e19e634b21dd6a8f131a2e943369c59c36c2271e' => 
    array (
      0 => '/var/www/html/themes/Frontend/Bare/frontend/plugins/seo/index.tpl',
      1 => 1537274534,
      2 => 'file',
    ),
    'cd68274301479d208207f1c1c534ff218c1cf75e' => 
    array (
      0 => '/var/www/html/themes/Frontend/Bare/frontend/search/ajax.tpl',
      1 => 1537274534,
      2 => 'file',
    ),
    '59fd2a9c510af33e24c2810f3222a75ced6d10bb' => 
    array (
      0 => '/var/www/html/themes/Frontend/Bare/frontend/listing/product-box/product-price.tpl',
      1 => 1537274534,
      2 => 'snippet',
    ),
  ),
  'nocache_hash' => '8706001165bb89ecfcd4876-53906141',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'sSearchResults' => 0,
    'search_result' => 0,
    'sArticle' => 0,
    'sSearchRequest' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bb89ecfeff0a1_18087130',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bb89ecfeff0a1_18087130')) {function content_5bb89ecfeff0a1_18087130($_smarty_tpl) {?><?php if (!$_smarty_tpl->tpl_vars['sSearchResults']->value['sResults']){?>

    
    
        <ul class="results--list">
            <li class="list--entry entry--no-results result--item"><?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'SearchAjaxNoResults','namespace'=>'frontend/search/ajax')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'SearchAjaxNoResults','namespace'=>'frontend/search/ajax'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Keine Suchergebnisse gefunden<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'SearchAjaxNoResults','namespace'=>'frontend/search/ajax'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</li>
        </ul>
    

<?php }else{ ?>

    
        <ul class="results--list">
            <?php  $_smarty_tpl->tpl_vars['search_result'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['search_result']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sSearchResults']->value['sResults']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['search_result']->key => $_smarty_tpl->tpl_vars['search_result']->value){
$_smarty_tpl->tpl_vars['search_result']->_loop = true;
?>

                
                
                    <li class="list--entry block-group result--item">
                        <a class="search-result--link" href="<?php echo $_smarty_tpl->tpl_vars['search_result']->value['link'];?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_result']->value['name'], ENT_QUOTES, 'utf-8', true);?>
">

                            
                            
                                <span class="entry--media block">
                                    <?php if ($_smarty_tpl->tpl_vars['search_result']->value['image']['thumbnails'][0]){?>
                                        <img srcset="<?php echo $_smarty_tpl->tpl_vars['search_result']->value['image']['thumbnails'][0]['sourceSet'];?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_result']->value['name'], ENT_QUOTES, 'utf-8', true);?>
" class="media--image">
                                    <?php }else{ ?>
                                        <img src="/themes/Frontend/Responsive/frontend/_public/src/img/no-picture.jpg" alt="<?php ob_start();?><?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'ListingBoxNoPicture','namespace'=>'frontend/search/ajax')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'ListingBoxNoPicture','namespace'=>'frontend/search/ajax'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo "Kein Bild";?><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'ListingBoxNoPicture','namespace'=>'frontend/search/ajax'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php $_tmp1=ob_get_clean();?><?php echo htmlspecialchars($_tmp1, ENT_QUOTES, 'utf-8', true);?>
" class="media--image">
                                    <?php }?>
                                </span>
                            

                            
                            
                                <span class="entry--name block">
                                    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escapeHtml'][0][0]->escapeHtml($_smarty_tpl->tpl_vars['search_result']->value['name']);?>

                                </span>
                            

                            
                            
                                <span class="entry--price block">
                                    <?php $_smarty_tpl->tpl_vars['sArticle'] = new Smarty_variable($_smarty_tpl->tpl_vars['search_result']->value, null, 0);?>
                                    
                                    <?php $_smarty_tpl->createLocalArrayVariable('sArticle', null, 0);
$_smarty_tpl->tpl_vars['sArticle']->value['has_pseudoprice'] = 0;?>
                                    <?php /*  Call merged included template "frontend/listing/product-box/product-price.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate("frontend/listing/product-box/product-price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('sArticle'=>$_smarty_tpl->tpl_vars['sArticle']->value), 0, '8706001165bb89ecfcd4876-53906141');
content_5bb89ecfe66238_98097843($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "frontend/listing/product-box/product-price.tpl" */?>
                                </span>
                            
                        </a>
                    </li>
                
            <?php } ?>

            
            
                <li class="entry--all-results block-group result--item">

                    
                    
                        <a href="<?php echo 'http://localhost/search';?>?sSearch=<?php echo urlencode($_smarty_tpl->tpl_vars['sSearchRequest']->value['sSearch']);?>
" class="search-result--link entry--all-results-link block">
                            <i class="icon--arrow-right"></i>
                            <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'SearchAjaxLinkAllResults','namespace'=>'frontend/search/ajax')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'SearchAjaxLinkAllResults','namespace'=>'frontend/search/ajax'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Alle Ergebnisse anzeigen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'SearchAjaxLinkAllResults','namespace'=>'frontend/search/ajax'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                        </a>
                    

                    
                    
                        <span class="entry--all-results-number block">
                            <?php echo $_smarty_tpl->tpl_vars['sSearchResults']->value['sArticlesCount'];?>
 <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'SearchAjaxInfoResults','namespace'=>'frontend/search/ajax')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'SearchAjaxInfoResults','namespace'=>'frontend/search/ajax'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Treffer<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'SearchAjaxInfoResults','namespace'=>'frontend/search/ajax'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                        </span>
                    
                </li>
            
        </ul>
    
<?php }?>
<?php }} ?><?php /* Smarty version Smarty-3.1.12, created on 2018-10-06 13:38:55
         compiled from "/var/www/html/themes/Frontend/Bare/frontend/listing/product-box/product-price.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5bb89ecfe66238_98097843')) {function content_5bb89ecfe66238_98097843($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_currency')) include '/var/www/html/engine/Library/Enlight/Template/Plugins/modifier.currency.php';
?>

<div class="product--price">

    
    
        <span class="price--default is--nowrap<?php if ($_smarty_tpl->tpl_vars['sArticle']->value['has_pseudoprice']){?> is--discount<?php }?>">
            <?php if ($_smarty_tpl->tpl_vars['sArticle']->value['priceStartingFrom']){?><?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'ListingBoxArticleStartsAt','namespace'=>'frontend/listing/box_article')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'ListingBoxArticleStartsAt','namespace'=>'frontend/listing/box_article'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
ab<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'ListingBoxArticleStartsAt','namespace'=>'frontend/listing/box_article'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php }?>
            <?php echo smarty_modifier_currency($_smarty_tpl->tpl_vars['sArticle']->value['price']);?>

            <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'Star','namespace'=>'frontend/listing/box_article')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'Star','namespace'=>'frontend/listing/box_article'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
*<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'Star','namespace'=>'frontend/listing/box_article'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        </span>
    

    
    
        <?php if ($_smarty_tpl->tpl_vars['sArticle']->value['has_pseudoprice']){?>
            <span class="price--pseudo">

                
                    <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'priceDiscountLabel','namespace'=>'frontend/listing/box_article')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'priceDiscountLabel','namespace'=>'frontend/listing/box_article'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'priceDiscountLabel','namespace'=>'frontend/listing/box_article'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                

                <span class="price--discount is--nowrap">
                    <?php echo smarty_modifier_currency($_smarty_tpl->tpl_vars['sArticle']->value['pseudoprice']);?>

                    <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'Star','namespace'=>'frontend/listing/box_article')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'Star','namespace'=>'frontend/listing/box_article'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
*<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'Star','namespace'=>'frontend/listing/box_article'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                </span>

                
                    <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'priceDiscountInfo','namespace'=>'frontend/listing/box_article')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'priceDiscountInfo','namespace'=>'frontend/listing/box_article'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'priceDiscountInfo','namespace'=>'frontend/listing/box_article'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                
            </span>
        <?php }?>
    
</div>
<?php }} ?>