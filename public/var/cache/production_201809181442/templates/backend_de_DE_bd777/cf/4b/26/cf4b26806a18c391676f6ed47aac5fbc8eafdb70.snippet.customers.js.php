<?php /* Smarty version Smarty-3.1.12, created on 2018-10-06 19:01:24
         compiled from "/var/www/html/themes/Backend/ExtJs/backend/index/model/customers.js" */ ?>
<?php /*%%SmartyHeaderCode:8448321825bb8ea648155d9-83580932%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cf4b26806a18c391676f6ed47aac5fbc8eafdb70' => 
    array (
      0 => '/var/www/html/themes/Backend/ExtJs/backend/index/model/customers.js',
      1 => 1537274534,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8448321825bb8ea648155d9-83580932',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bb8ea648310d1_23292815',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bb8ea648310d1_23292815')) {function content_5bb8ea648310d1_23292815($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Index.model.Customers', {
    extend: 'Ext.data.Model',
    fields: [
        //
        'customer', 'amount']
});
//
<?php }} ?>