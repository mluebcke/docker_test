<?php /* Smarty version Smarty-3.1.12, created on 2018-10-06 19:01:24
         compiled from "/var/www/html/themes/Backend/ExtJs/backend/index/model/theme_cache_warm_up.js" */ ?>
<?php /*%%SmartyHeaderCode:12530176225bb8ea64c01b27-70449371%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'db39ae09884e9e0a5d4204287f199cdec648b4e6' => 
    array (
      0 => '/var/www/html/themes/Backend/ExtJs/backend/index/model/theme_cache_warm_up.js',
      1 => 1537274534,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12530176225bb8ea64c01b27-70449371',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bb8ea64c1e612_39511209',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bb8ea64c1e612_39511209')) {function content_5bb8ea64c1e612_39511209($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Theme cache warm up model
 *
 * Loads stores that use themes
 */
//
Ext.define('Shopware.apps.Index.model.ThemeCacheWarmUp', {
    extend: 'Shopware.apps.Base.model.Shop'
});
//
<?php }} ?>