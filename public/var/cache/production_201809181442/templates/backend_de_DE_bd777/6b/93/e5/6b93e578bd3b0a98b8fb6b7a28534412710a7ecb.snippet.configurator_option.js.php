<?php /* Smarty version Smarty-3.1.12, created on 2018-10-06 19:01:42
         compiled from "/var/www/html/themes/Backend/ExtJs/backend/article/model/configurator_option.js" */ ?>
<?php /*%%SmartyHeaderCode:11304443985bb8ea76eef8c3-79981103%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6b93e578bd3b0a98b8fb6b7a28534412710a7ecb' => 
    array (
      0 => '/var/www/html/themes/Backend/ExtJs/backend/article/model/configurator_option.js',
      1 => 1537274534,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11304443985bb8ea76eef8c3-79981103',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bb8ea76f32677_16167038',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bb8ea76f32677_16167038')) {function content_5bb8ea76f32677_16167038($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Article
 * @subpackage Batch
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Model - Article models.
 * The article model contains all data about the article. Additional the model
 * contains the definition of the model associations. The article model
 * contains a proxy to define which request has to been sent to save, update and delete
 * the article.
 */
//
Ext.define('Shopware.apps.Article.model.ConfiguratorOption', {
    /**
     * Extends the standard Ext Model
     * @string
     */
    extend: 'Ext.data.Model',

    /**
     * The fields used for this model
     * @array
     */
    fields: [
        //
        { name: 'id', type: 'int', useNull: true },
        { name: 'groupId', type: 'int' },
        { name: 'active', type: 'boolean', defaultValue: false },
        { name: 'articleId', type: 'int' },
        { name: 'mediaId', type: 'int' },
        { name: 'name', type: 'string' },
        { name: 'position', type: 'int' }
    ],

    proxy: {
        /**
         * Set proxy type to ajax
         * @string
         */
        type:'ajax',

        /**
         * Configure the url mapping for the different
         * store operations based on
         * @object
         */
        api: {
            update: '<?php echo '/backend/Article/saveConfiguratorOption';?>',
            destroy: '<?php echo '/backend/Article/deleteConfiguratorOption';?>'
        }
    }
});
//
<?php }} ?>