<?php /* Smarty version Smarty-3.1.12, created on 2018-10-06 19:00:30
         compiled from "/var/www/html/themes/Backend/ExtJs/backend/base/application/Shopware.model.DataOperation.js" */ ?>
<?php /*%%SmartyHeaderCode:845308665bb8ea2e3c3505-59526033%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2fdaa91df2d9982ce94616bbb76a366972fa7ac3' => 
    array (
      0 => '/var/www/html/themes/Backend/ExtJs/backend/base/application/Shopware.model.DataOperation.js',
      1 => 1537274534,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '845308665bb8ea2e3c3505-59526033',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bb8ea2e3d1f46_31574216',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bb8ea2e3d1f46_31574216')) {function content_5bb8ea2e3d1f46_31574216($_smarty_tpl) {?>
//
//

Ext.define('Shopware.model.DataOperation', {

    extend:'Ext.data.Model',

    phantom: true,

    fields:[
        { name: 'success', type: 'boolean' },
        { name: 'request' },
        { name: 'error', type: 'string' },
        { name: 'operation' },
    ]
});
//
<?php }} ?>