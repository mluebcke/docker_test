<?php /* Smarty version Smarty-3.1.12, created on 2018-10-06 19:00:35
         compiled from "/var/www/html/themes/Backend/ExtJs/backend/base/attribute/field/Shopware.form.field.FormSingleSelection.js" */ ?>
<?php /*%%SmartyHeaderCode:20105129715bb8ea33a9ed50-75038433%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1591d01898a4d4d5e7ec390b7f42c2ddc8135e6d' => 
    array (
      0 => '/var/www/html/themes/Backend/ExtJs/backend/base/attribute/field/Shopware.form.field.FormSingleSelection.js',
      1 => 1537274534,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20105129715bb8ea33a9ed50-75038433',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bb8ea33aad856_52383833',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bb8ea33aad856_52383833')) {function content_5bb8ea33aad856_52383833($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category    Shopware
 * @package     Base
 * @subpackage  Attribute
 * @version     $Id$
 * @author      shopware AG
 */

//

Ext.define('Shopware.form.field.FormSingleSelection', {
    extend: 'Shopware.form.field.SingleSelection',
    alias: 'widget.shopware-form-field-form-single-selection',

    getComboConfig: function() {
        var me = this;
        var config = me.callParent(arguments);

        config.tpl = Ext.create('Ext.XTemplate',
            '<tpl for=".">',
                '<div class="x-boundlist-item">{name} - <i>{emailSubject} ({email})</i></div>',
            '</tpl>'
        );
        config.displayTpl = Ext.create('Ext.XTemplate',
            '<tpl for=".">',
                '{name} - {emailSubject} ({email})',
            '</tpl>'
        );
        return config;
    }
});<?php }} ?>