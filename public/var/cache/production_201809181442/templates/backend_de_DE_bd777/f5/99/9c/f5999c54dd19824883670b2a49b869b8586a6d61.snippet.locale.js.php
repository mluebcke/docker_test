<?php /* Smarty version Smarty-3.1.12, created on 2018-10-06 19:00:44
         compiled from "/var/www/html/themes/Backend/ExtJs/backend/login/store/locale.js" */ ?>
<?php /*%%SmartyHeaderCode:16985790285bb8ea3c99d948-70989753%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f5999c54dd19824883670b2a49b869b8586a6d61' => 
    array (
      0 => '/var/www/html/themes/Backend/ExtJs/backend/login/store/locale.js',
      1 => 1537274534,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16985790285bb8ea3c99d948-70989753',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bb8ea3c9abc74_02961660',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bb8ea3c9abc74_02961660')) {function content_5bb8ea3c9abc74_02961660($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Login
 * @subpackage Store
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */
Ext.define('Shopware.apps.Login.store.Locale', {
    extend: 'Ext.data.Store',
    autoLoad: true,
    model : 'Shopware.apps.Login.model.Locale'
});
<?php }} ?>