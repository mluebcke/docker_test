<?php /* Smarty version Smarty-3.1.12, created on 2018-10-06 19:00:33
         compiled from "/var/www/html/themes/Backend/ExtJs/backend/base/component/Shopware.window.ExpiredPluginWarning.js" */ ?>
<?php /*%%SmartyHeaderCode:2070258595bb8ea31b60bd5-41197180%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cc7ef6a24107e633aec970a7229f6b43c5a672da' => 
    array (
      0 => '/var/www/html/themes/Backend/ExtJs/backend/base/component/Shopware.window.ExpiredPluginWarning.js',
      1 => 1537274534,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2070258595bb8ea31b60bd5-41197180',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bb8ea31baf431_20479788',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bb8ea31baf431_20479788')) {function content_5bb8ea31baf431_20479788($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @version    $Id$
 * @author shopware AG
 */
//

Ext.define('Shopware.window.ExpiredPluginWarning', {
    extend: 'Enlight.app.Window',
    autoScroll: true,
    layout: 'fit',
    height: 430,
    width: 870,
    autoShow: true,
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'expired_plugins_popup/title','namespace'=>'backend/plugin_manager/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'expired_plugins_popup/title','namespace'=>'backend/plugin_manager/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin Warnung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'expired_plugins_popup/title','namespace'=>'backend/plugin_manager/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    footerButton: false,

    initComponent: function() {
        var me = this;

        me.items = [
            {
                xtype: 'form',
                items: [me.createContentPage()],
                dockedItems: [{
                    xtype: 'toolbar',
                    dock: 'bottom',
                    ui: 'shopware-ui',
                    cls: 'shopware-toolbar',
                    items: me.getWindowButtons()
                }]
            },
        ];

        me.callParent(arguments);
    },

    createContentPage: function () {
        return {
            xtype: 'container',
            html: '<style>.plugin-window { color: #475C6A; } .plugin-window h2 { color: #495B67;font-weight: bold;font-size: 24px;margin-bottom: 20px } .plugin-window p { margin-bottom: 20px;line-height: 140%;} .plugin-window img { width: calc(100% - 100px);padding: 50px;margin-top: 40px;position: absolute;top: 50%;left: 50%;transform: translateX(-50%); } .plugin-window .bar { width: 50%;float: left;position: relative; } .plugin-window .bar:last-child { margin-left: 50%; } .plugin-window strong { font-weight: bold; }</style><div class="plugin-window"><div class="bar"><img src="' + this.getImage() + '"></div><div class="bar"><div style="margin-top: 50px;margin-right: 30px;">' + this.getText() + '</div></div></div>'
        }
    },

    getImage: function() {
        return '/themes/Backend/ExtJs/backend/_resources/images/plugin_manager/warning.svg';
    },

    getText: function() {
        return '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'expired_plugins_popup/content','namespace'=>'backend/plugin_manager/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'expired_plugins_popup/content','namespace'=>'backend/plugin_manager/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<h2>Warnung!</h2><p>Mindestens eine Lizenz der von Dir eingesetzten Plugins ist leider abgelaufen. Überprüfe die Lizenzen in Deinem <a href=https://account.shopware.com target=_blank>Shopware Account</a> unter <strong style=font-weight: bold>„Lizenzen“</strong>. Andernfalls musst Du das entsprechende Plugin aus Deinem Shop löschen, da Du nicht mehr berechtigt bist, es einzusetzen.</p><p>Bei Fragen stehen wir Dir unter <a href=mailto:sales@shopware.com>sales@shopware.com</a> oder telefonisch unter <a href=tel:+4902555928850>+49 (0) 2555 928850</a> zur Verfügung. </p><p>Dein Shopware-Team</p><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'expired_plugins_popup/content','namespace'=>'backend/plugin_manager/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
';
    },

    getWindowButtons: function() {
        var me = this;

        return [
            '->',
            {
                xtype: 'button',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'expired_plugins_popup/cancel','namespace'=>'backend/plugin_manager/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'expired_plugins_popup/cancel','namespace'=>'backend/plugin_manager/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Schließen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'expired_plugins_popup/cancel','namespace'=>'backend/plugin_manager/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                scope: me,
                cls: 'secondary',
                handler: function () {
                    me.destroy();
                }
            },
            {
                xtype: 'button',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'expired_plugins_popup/goToPluginManager','namespace'=>'backend/plugin_manager/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'expired_plugins_popup/goToPluginManager','namespace'=>'backend/plugin_manager/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zum Plugin-Manager<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'expired_plugins_popup/goToPluginManager','namespace'=>'backend/plugin_manager/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                scope: me,
                cls: 'primary',
                handler: function () {
                    Shopware.app.Application.addSubApplication({
                        name: 'Shopware.apps.PluginManager',
                        action: 'ExpiredPlugins'
                    });

                    me.destroy();
                }
            }
        ];
    }
});<?php }} ?>