<?php /* Smarty version Smarty-3.1.12, created on 2018-10-06 19:00:34
         compiled from "/var/www/html/themes/Backend/ExtJs/backend/base/component/element/media_selection.js" */ ?>
<?php /*%%SmartyHeaderCode:6607326235bb8ea327226e5-09750944%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e380bf126703b9c1d7876a34ae14fd9704ac77e7' => 
    array (
      0 => '/var/www/html/themes/Backend/ExtJs/backend/base/component/element/media_selection.js',
      1 => 1537274534,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6607326235bb8ea327226e5-09750944',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bb8ea3272fde3_57068548',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bb8ea3272fde3_57068548')) {function content_5bb8ea3272fde3_57068548($_smarty_tpl) {?>/*
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Base
 * @subpackage Component
 * @version    $Id$
 * @author shopware AG
 */
Ext.define('Shopware.apps.Base.view.element.MediaSelection', {
    extend: 'Shopware.MediaManager.MediaSelection',
    alias: [
        'widget.base-element-media',
        'widget.base-element-mediaselection',
        'widget.base-element-mediafield',
        'widget.base-element-mediaselectionfield',
        'widget.config-element-media',
        'widget.config-element-mediaselection'
    ]
});
<?php }} ?>