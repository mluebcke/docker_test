<?php /* Smarty version Smarty-3.1.12, created on 2018-10-06 19:00:32
         compiled from "/var/www/html/themes/Backend/ExtJs/backend/base/store/translation.js" */ ?>
<?php /*%%SmartyHeaderCode:13570812185bb8ea3004a611-57462367%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cbcc45e51415c62eca49f43dff27c32fc8919e18' => 
    array (
      0 => '/var/www/html/themes/Backend/ExtJs/backend/base/store/translation.js',
      1 => 1537274534,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13570812185bb8ea3004a611-57462367',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bb8ea3006f7a7_37604461',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bb8ea3006f7a7_37604461')) {function content_5bb8ea3006f7a7_37604461($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Base
 * @subpackage Store
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Store - Global Stores and Models
 *
 * todo@all: Documentation
 */
Ext.define('Shopware.apps.Base.store.Translation', {
    extend: 'Ext.data.Store',

    alternateClassName: 'Shopware.store.Translation',
    storeId: 'base.Translation',
    model : 'Shopware.apps.Base.model.Shop',
    pageSize: 1000,

    remoteSort: true,
    remoteFilter: true,

    proxy:{
        type:'ajax',
        url: '<?php echo '/backend/base/getShops';?>',
        reader:{
            type:'json',
            root:'data',
            totalProperty:'total'
        }
    },

    filters: [{
        property: 'default',
        value: false
    }]
}).create();

<?php }} ?>