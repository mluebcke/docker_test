<?php /* Smarty version Smarty-3.1.12, created on 2018-10-06 19:00:34
         compiled from "/var/www/html/themes/Backend/ExtJs/backend/base/attribute/field_handler/Shopware.attribute.DateFieldHandler.js" */ ?>
<?php /*%%SmartyHeaderCode:21446308835bb8ea32bb8c56-33996409%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f028fd1c3a2bc3f1fc59e942570dc3ad0e2d52f3' => 
    array (
      0 => '/var/www/html/themes/Backend/ExtJs/backend/base/attribute/field_handler/Shopware.attribute.DateFieldHandler.js',
      1 => 1537274534,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21446308835bb8ea32bb8c56-33996409',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5bb8ea32bcd151_24100535',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bb8ea32bcd151_24100535')) {function content_5bb8ea32bcd151_24100535($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category    Shopware
 * @package     Base
 * @subpackage  Attribute
 * @version     $Id$
 * @author      shopware AG
 */

Ext.define('Shopware.attribute.DateFieldHandler', {
    extend: 'Shopware.attribute.FieldHandlerInterface',

    supports: function(attribute) {
        return (attribute.get('columnType') == 'date');
    },

    create: function(field, attribute) {
        field.xtype = 'base-element-date';
        field.submitFormat = 'Y-m-d';
        field.dateCfg = { submitFormat: 'Y-m-d' };
        return field;
    }
});<?php }} ?>